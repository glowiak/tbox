# TBox

TBox is simple shell-based desktop for UNIX-like operating systems.

### NOTE

This desktop is made for suckless extremists, that say that display servers are bloat. TBox would fit in their criteria - it's made in Shell and with Vim.

### Installation

Git clone this repository, and type:

	make install

and if you want to use it in X11, needs xterm:

	make install_x11

You can customize DESTDIR and PREFIX variables. The X11 version will work only with /usr/local and /usr PREFIXes!

### Contribution

Please, fork and/or contribute to the project.

Tho it's developed just for fun, it can become bigger and more useful.

### Copyright

TBox is licensed under 3-clause BSD license and is registered trademark of no one.

### Features

-Windows (not the os)

-TBTK (TBox ToolKit) to create (yet simple) apps

-Simple and lightweight (uses POSIX-complain shell)

### What I want to add

[NOT IMPLEMENTED YET] complete all internal programs

[DONE] make possible to edit files with Text Editor

[NOT IMPLEMENTED YET] make TBox follow UNIX philozophy - move all internal programs into .tboxprog scripts in other directory

[NOT SURE IF IT WOULD BE GOOD CHANGE] move from TBTK to dialog as it's easier to do some tasks like terminal or fully working Text Editor there

[NOT IMPLEMENTED YET] make possible moving windows

[NOT IMPLEMENTED YET] add colors to TBox

### Shell compatibility

TBox is written is Shell, but there are many shells. Which ones are supported or not?

Supported:

-all POSIX-complaint shells except dash

Unsupported:

-dash

-not-POSIX-complaint shells like tcsh

### Credits

Made with vim
