#!/bin/sh
# TBox - a try to make a shell-based TUI desktop

clear

export NAME=tbox
export PRETTY_NAME="TBox"
export VERSION=1.0
export PV="$PRETTY_NAME v$VERSION"
export TMP=/tmp/$NAME-$VERSION

rm -rf $TMP
mkdir -p $TMP

msg_window()
{
	cat << "EOF"
####################### Message ##########################
#                                                        #
EOF
	echo "# $*"
	cat << "EOF"
#                                                        #
##########################################################
EOF
	echo
}

ask_window()
{
	cat << "EOF"
#################### Question ########################
#                                                    #
#    The Program wants some information from You     #
EOF
	echo "#   Question: $(get_window_var QUESTION)"
	cat << "EOF"
#                                                    #
#            Available Anwsers:                      #
EOF
	for anum in $(get_window_var ANWSER_NUMBER)
	do
		echo "#   $anum: $(get_window_var ANWSER_${anum})"
		cat << "EOF"
#                                                    #
EOF
	done
	cat << "EOF"
# Please insert information at bottom of the screen  #
#                                                    #
######################################################
EOF
	echo
	echo
	echo
	read BOTTOM
	set_window_var COMPLETED_ANWSER $BOTTOM
}

system_bar_top()
{
	clear
	echo "### $PV ############################################################################ $(date) ###"
	echo
}

set_window_var()
{
	VARIABLE=$1
	VALUE="$(echo $* | sed "s/$VARIABLE //")"
	echo "$VALUE" > $TMP/$VARIABLE.VAR.TXT
}

get_window_var()
{
	VARIABLE=$1
	cat $TMP/$VARIABLE.VAR.TXT
}

program_about()
{
	cat << "EOF"
########## About ##############
#                             #
# TBox version 1.0            #
# TBox is a simple, shell-bas #
# ed TUI Desktop for *NIX.    #
#                             #
# TBox was made by glowiak    #
# and it's licensed under     #
# 3-clause BSD license        #
#                             #
#       ######                #
#       # OK #                #
#       ######                #
#                             #
###############################
EOF
	read no
}

program_file_browser()
{
	clear
	system_bar_top
	case "$1" in
		"")
			export LSDIR=$HOME
			;;
		*)
			export LSDIR="$*"
			if [ -f "$LSDIR" ]; then
				clear
				set_window_var QUESTION What do you want to do with this file?
				set_window_var ANWSER_NUMBER 1 2
				set_window_var ANWSER_1 Open with Text Editor
				set_window_var ANWSER_2 Run the file
				ask_window
				case "$(get_window_var COMPLETED_ANWSER)" in
					1)
						clear
						program_textedit view "$LSDIR"
						;;
					2)
						clear
						"./$LSDIR" || "$LSDIR"
						;;
					*)
						;;
				esac
			fi
			;;
	esac
	clear
	system_bar_top
	cat << "EOF"
################# File Browser #################
#                                              #
EOF
	for i in $(ls $LSDIR)
	do
		if [ -f "$LSDIR/$i" ]
		then
			export FBTYPE=File
		fi
		if [ -d "$LSDIR/$i" ]
		then
			export FBTYPE=Directory
		fi
		echo "# ${FBTYPE}: $LSDIR/$i"
		cat << "EOF"
#                                              #
EOF
	done
	cat << "EOF"
# Type directory to list or hit enter to exit  #
################################################
EOF
	read no
	clear
	case "$no" in
		"")
			;;
		*)
			program_file_browser $no
			;;
	esac
}

program_textedit()
{
	clear
	system_bar_top
	OPTION=$1
	REST="$(echo $* | sed "s/$OPTION //")"
	case "$OPTION" in
		view)
			cat << "EOF"
############ Text Editor #############
#                                    #
EOF
			echo "# File $REST:"
			cat << "EOF"
#                                    #
EOF
			cat $REST
			cat << "EOF"
#                                    #
######################################
EOF
			read nope
			;;
		new)
			rm -rf $TMP/TEXTEDIT.SAV
			touch $TMP/TEXTEDIT.SAV
			cat << "EOF"
########## Text Editor ###############
#                                    #
#  Editing a file. You can type text #
# here. Type 'tesave' to save file.  #
EOF
			while [ -f $TMP/TEXTEDIT.SAV ]
			do
				read tetext
				if [ "$tetext" == "tesave" ]
				then
					cat << "EOF"
#                                    #
######################################
EOF
					echo
					echo -n "Enter file name to save to: "
					read savedfile
					cat $TMP/TEXTEDIT.SAV > "$savedfile"
					rm -rf $TMP/TEXTEDIT.SAV
				fi
				if [ ! "$tetext" == "tesave" ]
				then
					echo "$tetext" >> $TMP/TEXTEDIT.SAV
				fi
			done
			;;
		line-edit)
			rm -rf $TMP/TELESTATUS.TMP
			touch $TMP/TELESTATUS.TMP
			cat << "EOF"
########## Text Editor ###############
#                                    #
# Line-editing a file. You can type  #
# here sed commands to execute. Type #
# 'tesave' to save file.             #
EOF
			while [ -f $TMP/TELESTATUS.TMP ]
			do
				read teletext
				if [ "$teletext" == "tesave" ]
				then
					cat << "EOF"
#                                    #
######################################
EOF
					echo
					echo -n "Enter file name to save changes to: "
					read changedfile
					export IFS='*'
					for i in $(cat $TMP/TELESTATUS.TMP)
					do
						sed -i "$i" $changedfile || gsed -i "$i" $changedfile
					done
					unset IFS
					unset i
					rm -rf $TMP/TELESTATUS.TMP
				fi
				if [ ! "$teletext" == "tesave" ]
				then
					echo "${teletext}*" >> $TMP/TELESTATUS.TMP
				fi
			done
			;;
		about)
			cat << "EOF"
######### About Text Editor #########
#                                   #
# TextEdit is a Text Editor bundled #
# with TBox Desktop.                #
#                                   #
#         Credits:                  #
#        made with vim              #
#         uses TBTK                 #
#                                   #
# This is early version. Please     #
# consider, this program may be     #
# incomplete or broken.             #
#                                   #
#####################################
EOF
			read no
			;;
		menu)
			set_window_var QUESTION Select action for Text Editor
			set_window_var ANWSER_NUMBER 1 2 3 4
			set_window_var ANWSER_1 New file
			set_window_var ANWSER_2 View file
			set_window_var ANWSER_3 Line-edit file
			set_window_var ANWSER_4 About Text Editor
			ask_window
			case "$(get_window_var COMPLETED_ANWSER)" in
				1)
					clear
					system_bar_top
					program_textedit new
					;;
				2)
					clear
					system_bar_top
					set_window_var QUESTION Enter file name to view
					set_window_var ANWSER_NUMBER 1
					set_window_var ANWSER_1 .
					ask_window
					THEFILE="$(get_window_var COMPLETED_ANWSER)"
					if [ -f "$THEFILE" ]
					then
						program_textedit view $THEFILE
					fi
					if [ ! -f "$THEFILE" ]
					then
						clear
						system_bar_top
						msg_window "The file doesn't exist. Exiting."
						sleep 2
					fi
					;;
				3)
					clear
					system_bar_top
					program_textedit line-edit
					;;
				4)
					clear
					system_bar_top
					program_textedit about
					;;
				*)
					;;
			esac
			;;
		*)
			;;
	esac
	clear
}

program_settings()
{
	clear
	system_bar_top
	set_window_var QUESTION What do you want to do?
	set_window_var ANWSER_NUMBER 1
	set_window_var ANWSER_1 Run local app
	ask_window
	case "$(get_window_var COMPLETED_ANWSER)" in
		1)
			clear
			system_bar_top
			program_settings_run_local_app
			;;
		*)
			;;
	esac
}

program_settings_run_local_app()
{
	clear
	system_bar_top
	set_window_var QUESTION Enter app directory
	set_window_var ANWSER_NUMBER 1
	set_window_var ANWSER_1 .
	ask_window
	export LOCAL_APP_PATH="$(get_window_var COMPLETED_ANWSER)"
	clear
	system_bar_top
	set_window_var QUESTION Select Application to run
	export NUM1=0
	export NUM2=
	for i in $(ls $LOCAL_APP_PATH | sed "s/.tboxprog//g")
	do
		NUM1=$(expr $NUM1 + 1)
		NUM2="$NUM2 $NUM1"
		set_window_var ANWSER_${NUM1} "$i"
		set_window_var SLAP_ANWS${NUM1} "$i"
	done
	set_window_var ANWSER_NUMBER $NUM2
	ask_window
	export FINAL_NUMBER=$(get_window_var COMPLETED_ANWSER)
	export LAPP_FILE="$(get_window_var SLAP_ANWS${FINAL_NUMBER})"
	. "$(readlink -f $LOCAL_APP_PATH/$LAPP_FILE.tboxprog)"
	clear
	system_bar_top
	program
	read no
}
clear
system_bar_top
msg_window "Welcome to $PV! Press enter to continue!"
read PRESS_ENTER
while true
do
	clear
	system_bar_top
	set_window_var QUESTION What do you want to do?
	set_window_var ANWSER_NUMBER 1 2 3 4 5 6
	set_window_var ANWSER_1 Browse Your Files
	set_window_var ANWSER_2 Run Terminal
	set_window_var ANWSER_3 Run Text Editor
	set_window_var ANWSER_4 Settings
	set_window_var ANWSER_5 About $PV
	set_window_var ANWSER_6 Exit $PV
	ask_window
	case "$(get_window_var COMPLETED_ANWSER)" in
		1)
			clear
			system_bar_top
			program_file_browser
			;;
		2)
			clear
			system_bar_top
			program_terminal
			;;
		3)
			clear
			system_bar_top
			program_textedit menu
			;;
		4)
			clear
			system_bar_top
			program_settings
			;;
		5)
			clear
			system_bar_top
			program_about
			;;
		6)
			exit
			;;
	esac
done
read __STOP__
