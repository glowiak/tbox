DESTDIR=	
PREFIX=	/usr/local

all:
	@echo targets: install install_x11

install:
	cp ./tbox.sh ${DESTDIR}${PREFIX}/bin/tbox
	chmod 775 ${DESTDIR}${PREFIX}/bin/tbox

install_x11: install
	cp ./tbox_x11.sh ${DESTDIR}${PREFIX}/bin/tbox_x11
	chmod 775 ${DESTDIR}${PREFIX}/bin/tbox_x11
